# FBULIDER #

Fbuilder is a builder for frontend, which is based on [Gulp.js](http://gulpjs.com/). 

# Features #
* jade
* sass
* bootstrap

# Installation #
You need to install Node.js with version equal to 4.x.x or higher.
```
npm i -g npm
```

Next you need to install gulp globally. (You may need rights of superuser or administrator).

```
npm install -g gulp
```

Download [FBULIDER](https://bitbucket.org/zorin_e/fbuilder/get/master.zip) and unzip it in the working directory. Then install dependencies.

```
npm install && bower install
```

After setting up the project, execute the following command that init project:

```
gulp dev:create
```

For start watch project execute:
```
gulp
```