console.log('hello!');

$(function() {
	// calculator
	if($('.calculator').length > 0) {
		var percent = 0.02,
			days = $('.calculator__action .calculator__range--days').data('param').value;
	    $('.calculator__action .calculator__range').each(function() {
	    	var t = $(this),
	    		data = $(this).data('param');

	    	$(this).slider({
				value: data.value,
				min: data.min,
				max: data.max,
				step: data.step,
				animate: true,
				slide: function( event, ui ) {
					if(ui.value === data.max) {
						t.find('.ui-slider-handle').addClass('ui-slider-handle__last');
					}
					else {
						t.find('.ui-slider-handle').removeClass('ui-slider-handle__last');
					}
					
					t.find(".calculator__amount span:first").text(ui.value+' ');

					days = parseInt(
						t.parents('.calculator__action').find(".calculator__range--days .calculator__amount span:first").text()
						,10);

					var value = parseInt(
						(!t.hasClass('calculator__range--days') ? 
							ui.value
							:
							t.parent().find('.calculator__range .calculator__amount span:first').text()
						)
						, 10);

					// change own
					t.parent().find('.calculator__bottom .calculator__your-own').text(
						value + (value * percent) * parseInt(days, 10)
					);

					// change date
					console.log(typeof t.find('.calculator__amount span:first').text());
					if(t.hasClass('calculator__range--days')) {
						t.parent().find('.calculator__bottom .calculator__date').text(
							addDays(parseInt(t.find('.calculator__amount span:first').text(), 10))
						);
					}
				}    		
	    	})
	    	.each(function() {
				var vals = data.max - data.min;
				// var i = 0;

				console.log(data.stepStop+':');

				for (var i = 0; i <= vals; (i == data.stepStop ? i+=data.stepLabel-data.step:i+=data.stepLabel)) {
					var el = $('<label>'+(i+data.step)+'</label>').css('left',(i/vals*100)+'%');
					t.append(el);
				}
			  
			});

			t.find(".calculator__amount span:first").text(t.slider('value')+' ');

			if(t.hasClass('calculator__range--days')) {
				t.parent().find('.calculator__bottom .calculator__date').text(addDays(data.value));
			}
			else {
				t.parent().find('.calculator__bottom .calculator__your-own').text(
					t.slider('value') + (t.slider('value') * percent) * parseInt(days, 10)
				);
			}
	    });		
	}
	

	// toggle map
	$('.toggle__item').on('click', function() {
		var index = $(this).index();

		// toggle change
		$(this).parent().find('.toggle__item--active').removeClass('toggle__item--active');
		$(this).addClass('toggle__item--active');

		// content change
		$(this).parents('.block').next('.toggle-me').find('.toggle-me__item--active').removeClass('toggle-me__item--active')
		.parent().find('.toggle-me__item').eq(index).addClass('toggle-me__item--active');
	});

	// reviews
	$(".reviews.owl-carousel").owlCarousel({
		items:4,
        // merge:true,
        loop:true,
        margin:86,
        video:true,
        lazyLoad:true,
        responsive:{
        	0: {
        		items:1
        	},
        	480:{
                items:2
            },
        	767:{
                items:3
            },
        	992: {
        		items:4
        	}
        }
	});

	// fancybox video
	$('.item-video__link').fancybox({
		type:'iframe',
        allowfullscreen: 'true',
        openEffect  : 'none',
        closeEffect : 'none',
        helpers : {
            media : {}
        }
    });

 	// swiper answer
 	var sliderAdverts = new Swiper('.answers__swiper .swiper-container', {
 		slidesPerView: 1,
        spaceBetween: 30,

        onInit: function() {
        	$('.answers__swiper').parents('.answers').find('.tabs__item').eq(0).addClass('tabs__item--active');
        },

        onSlideChangeStart: function(){
            $('.answers__swiper').parents('.answers').find('.tabs__item').removeClass('tabs__item--active');
            $('.answers__swiper').parents('.answers').find('.tabs__item').eq(sliderAdverts.activeIndex).addClass('tabs__item--active');
        },

        breakpoints: {
        	991: {
        		slidesPerView: 2
        	},

        	480: {
        		slidesPerView: 1
        	}
        }
    });

    // swiper answer nav
    $('.answers .tabs__item').click(function(){
        sliderAdverts.slideTo($(this).index());
        $('.answers .tabs__item').removeClass('tabs__item--active');
		$(this).addClass('tabs__item--active')
    });

    // slide answers
    $('.answers__question').on('click', function() {
    	if(!$(this).hasClass('answers__question--active')) {
    		// $(this).parent().find('.answers__question--active').removeClass('answers__question--active').next().stop().slideUp();
    		$(this).addClass('answers__question--active').next().stop().slideDown();
    	}
    	else {
    		$(this).removeClass('answers__question--active').next().stop().slideUp();	
    	}
    	

    });

    // swiper news
 	var sliderNews = new Swiper('.news .swiper-container', {
 		slidesPerView: 3,
        spaceBetween: 30,

        breakpoints: {
        	991: {
        		slidesPerView: 2
        	},

        	480: {
        		slidesPerView: 1
        	}
        }
    });

    // swiper answer
 	var sliderArticles = new Swiper('.articles__swiper > .swiper-container', {
 		slidesPerView: 1,
        spaceBetween: 30,

        onInit: function() {
        	$('.articles__swiper').parents('.articles').find('.tabs__item').eq(0).addClass('tabs__item--active');
        },

        onSlideChangeStart: function(){
            $('.articles__swiper').parents('.articles').find('.tabs__item').removeClass('tabs__item--active');
            $('.articles__swiper').parents('.articles').find('.tabs__item').eq(sliderArticles.activeIndex).addClass('tabs__item--active');
        }
    });

    // swiper answer nav
    $('.articles .tabs__item').click(function(){
        sliderArticles.slideTo($(this).index());
        $('.articles .tabs__item').removeClass('tabs__item--active');
		$(this).addClass('tabs__item--active')
    });

    // choose city
    $(document).on('click touchstart', '[data-show]', function(event) {
        var t = $(this);

        if($(t.data('show')).is(':hidden')) {
            $(t.data('show')).stop().slideDown('slow');
        }
        else {
            $(t.data('show')).stop().slideUp('slow');
        }

       return false;
    });

    // close
    $(document).on('click touchstart', '[data-action]', function(event) {
        var t = $(this);

        if(t.data('action') === 'close') {
            t.parents('.your-city').css({'opacity':'0','transform':'translateY(-20px)'});

            var timer = setInterval(function() {
                if(t.parents('.your-city').css('opacity') == 0) {
                    t.parents('.your-city').hide();
                    clearInterval(timer);
                }
            }, 100);
        }
    });

    // cities list
    $(document).on('click touchend', '.cities__item', function(event) {
        var t = $(this);

        if(t.find('.cities__list').is(':hidden')) {
            t.parents('.cities').find('.cities__list').stop().slideUp('slow');
            t.find('.cities__list').stop().slideDown('slow');

            return false;
        }
        else if($(event.target).closest('.cities__list').length !== 1) {
            t.find('.cities__list').stop().slideUp('slow');

            return false;
        }
    });

    // google map settings
    if(Modernizr.touchevents && $('.map').length > 0) {
        var googleSettings = function() {
            var lock = false;

            $(document).scroll(function() {
                if($(document).scrollTop() >= $('.map').offset().top && !lock) {
                    map.draggable = lock = true;
                }
                else {
                    lock = false;
                    map.draggable = !("ontouchend" in document);
                }
            });
        }();
    }
});

// add days for calculator
function addDays(days) {
	var now = new Date();
	var date;

	now.setDate(now.getDate() + days);
	var month = now.getMonth()+1;
	date = now.getDate()+'.'+month+'.'+now.getFullYear();

	return date;
};

// google map
var map;
function initMap() {
    var locations = [
      ['ост. «Электротехников» (БЦ "Единство", 62/04)', 55.720615, 52.378713],
      ['ТЦ "Меркурий" (ул. Ш. Усманова, 39)', 55.737970, 52.384990],
      ['ост. "Пушкинская", ТЦ "На Пушкинской"', 55.748706, 52.400165],
      ['ост. "Студенческая" (ТЦ "Челны", 8/32)', 55.687414, 52.305680],
      ['ост. Глобус ("ТЦ Глобус", 30/10а)', 55.745567, 52.425511],
      ['ост. "Домостроителей" (пр. Сююмбике, 49-й к-с)', 55.762556, 52.426544],
      ['ост. "мкр. Бумажников" (ЗЯБ, 17/08)', 55.700888, 52.332459],
      ['ост. ДК "Камаза" (ТЦ "Октябрьский", 25/01)', 55.754341, 52.431412],
      ['ост. "40 к-с" (пр. Чулман, 53)', 55.754341, 52.376799]
    ];

    var styles = [
        {
            "featureType": "landscape",
            "stylers": [
                {
                    "hue": "#FFBB00"
                },
                {
                    "saturation": 43.400000000000006
                },
                {
                    "lightness": 37.599999999999994
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.highway",
            "stylers": [
                {
                    "hue": "#FFC200"
                },
                {
                    "saturation": -61.8
                },
                {
                    "lightness": 45.599999999999994
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "stylers": [
                {
                    "hue": "#FF0300"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 51.19999999999999
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "road.local",
            "stylers": [
                {
                    "hue": "#FF0300"
                },
                {
                    "saturation": -100
                },
                {
                    "lightness": 52
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "water",
            "stylers": [
                {
                    "hue": "#0078FF"
                },
                {
                    "saturation": -13.200000000000003
                },
                {
                    "lightness": 2.4000000000000057
                },
                {
                    "gamma": 1
                }
            ]
        },
        {
            "featureType": "poi",
            "stylers": [
                {
                    "hue": "#00FF6A"
                },
                {
                    "saturation": -1.0989010989011234
                },
                {
                    "lightness": 11.200000000000017
                },
                {
                    "gamma": 1
                }
            ]
        }
    ];

    var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

    map = new google.maps.Map(document.getElementById('map'), {
      scrollwheel: false,
      center: new google.maps.LatLng(55.7134825, 52.2951643),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      draggable: !("ontouchend" in document)
    });

    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    var image = {
    	url: '../img/marker.png'
    };

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
    	icon: image,
        map: map
      });

      bounds.extend(marker.position);

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }

    map.fitBounds(bounds);
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');
};